package com.ruoyi.framework.interceptor;

public interface DecryptUtil {
    /**
     * 解密
     *
     * @param result
     * @param <T>
     * @return
     * @throws IllegalAccessException
     */
    <T> T decrypt(T result) throws Exception;
}
