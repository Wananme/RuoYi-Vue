package com.ruoyi.common.annotation;

import com.ruoyi.common.enums.DesensitionType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Desensitization {
    
    DesensitionType type();

    String[] attach() default "";
}