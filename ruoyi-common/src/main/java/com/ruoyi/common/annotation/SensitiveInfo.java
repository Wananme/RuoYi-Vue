package com.ruoyi.common.annotation;

import com.ruoyi.common.enums.SensitiveType;

import java.lang.annotation.*;
/*** 注解类
 *
 * @author liqun.long
 */
@Target({ElementType.FIELD,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface SensitiveInfo {
    public SensitiveType type();
}